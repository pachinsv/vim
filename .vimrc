syntax on
set cursorline
set number
set relativenumber
set scrolloff=1
set sidescroll=1
set wildmenu
set ruler
set nohlsearch
"set laststatus=2
set linebreak
set display=truncate
set noerrorbells
set title
set nowrap
set incsearch
set ignorecase
set smartcase
set nocompatible
set autoread
set showcmd
set dir=~/.vim/swapfiles//
"set shell=/usr/local/bin/fish
set wildignore+=.pyc

" Indentation
set expandtab
set autoindent
set tabstop=4
set softtabstop=4
set shiftwidth=4
set shiftround

" Vim Plug
call plug#begin()
Plug 'cohama/lexima.vim'
Plug 'janko-m/vim-test'
Plug 'justinmk/vim-sneak'
Plug 'joshdick/onedark.vim'
Plug 'mattn/emmet-vim'
Plug 'scrooloose/nerdtree'
Plug 'stephpy/vim-php-cs-fixer'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-fugitive'
Plug 'junegunn/gv.vim'
Plug 'tpope/vim-repeat'
" Plug 'neomake/neomake'
Plug 'tpope/vim-commentary'
Plug 'sodapopcan/vim-twiggy'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'easymotion/vim-easymotion'
Plug 'vim-airline/vim-airline'
call plug#end()

colorscheme onedark
"set rtp+=/usr/local/opt/fzf

nmap <silent> t<C-n> :TestNearest<CR>
nmap <silent> t<C-f> :TestFile<CR>
nmap <silent> t<C-s> :TestSuite<CR>
nmap <silent> t<C-l> :TestLast<CR>
nmap <silent> t<C-g> :TestVisit<CR>

nmap <leader>gs :Gstatus<CR>
nmap <leader>ag :Ag<CR>
nmap <leader>fa :Files<CR>
nmap <leader>f :GFiles<CR>
nmap <leader>b :Buffers<CR>

" Abre NERDTree al iniciar Vim desde un directorio.
"autocmd StdinReadPre * let s:std_in=1
"autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

" Cierra Vim al no tener ningun buffer abierto.
"autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
map <C-n> :NERDTreeToggle<CR>
let NERDTreeIgnore = ['\.pyc$', '__pycache__', '\~$']

"NeoMake
" call neomake#configure#automake('nw', 750)
" let g:neomake_php_phpcs_args_standard='PSR12'
